/**
 * Created by vadim24816 on 5/2/14.
 */
var Presentation = (function(){

	var
        PRESENTATION_CLASS = 'presentation',
        CURRENT_PRESENTATION_CLASS = 'presentation_state_current',
        SLIDE_CLASS = 'slide',
        CURRENT_SLIDE_CLASS = 'slide_state_current',
        CURRENT_SLIDE_ON_CURRENT_PRESENTATION_CLASS = CURRENT_PRESENTATION_CLASS + ' ' + CURRENT_SLIDE_CLASS,
        PROGRESS_BAR_CLASS = 'progress__bar',
        PROGRESS_CLASS = 'progress',

        PRESENTATION_SELECTOR = '.' + PRESENTATION_CLASS,
        SLIDE_SELECTOR = '.' + SLIDE_CLASS,
        SLIDE_ON_CURRENT_PRESENTATION_SELECTOR = '.' + CURRENT_PRESENTATION_CLASS + ' '  + '.' + SLIDE_CLASS,
        CURRENT_SLIDE_SELECTOR = '.' + CURRENT_SLIDE_CLASS,
        CURRENT_PRESENTATION_SELECTOR = '.' + CURRENT_PRESENTATION_CLASS,
        CURRENT_SLIDE_ON_CURRENT_PRESENTATION_SELECTOR = '.' + CURRENT_PRESENTATION_CLASS + ' ' +  '.' + CURRENT_SLIDE_CLASS,
        HOME_SLIDE_ON_CURRENT_PRESENTATION_SELECTOR = CURRENT_PRESENTATION_SELECTOR + ' .slide:first-of-type',
        END_SLIDE_SELECTOR = CURRENT_PRESENTATION_SELECTOR + ' .slide:last-of-type',
        PROGRESS_BAR_SELECTOR = '.' + PROGRESS_BAR_CLASS,
        PROGRESS_BAR_ON_CURRENT_PRESENTATION_SELECTOR = CURRENT_PRESENTATION_SELECTOR + ' ' + PROGRESS_BAR_SELECTOR,
        PROGRESS_SELECTOR = '.' + PROGRESS_CLASS,


    // Configurations defaults, can be overridden at initialization time
		config = {

			// The "normal" size of the presentation, aspect ratio will be preserved
			// when the presentation is scaled to fit different resolutions
			width: 450,
			height: 350,

			// Display a presentation progress bar
			progress: true,

			// Display the page number of the current slide
			slideNumber: false,

			// Enable keyboard shortcuts for navigation
			keyboard: true,

            // Отступы у слайда сверху и снизу
            slidePaddingTopBottom: 10,
            slidePaddingLeftRight: 5,

        }

    function getConfig(){
        return config;
    }

	/**
	 * Init
	 *
	 */
	function initialize( options ) {

		extend( config, options );

        layout();

        // Переходим по введенному URL к выбранной презентации и слайду
        navigateToURL();
    }

	/**
	 * Extend object a with the properties of object b.
	 * If there's a conflict, object b takes precedence.
	 */
	function extend( a, b ) {

		for( var i in b ) {
			a[ i ] = b[ i ];
		}

	}

    // Slide exists

    /**
     *
     * @param slide_as_object_or_id_or_index {Object|String|Number}
     * @returns {boolean}
     */
    function isSlideExists(slide_as_object_or_id_or_index){

        var is_slide = false;
        // Если передан слайд - проверяем его как объект
        if (is_slide = isSlide(slide_as_object_or_id_or_index)) return true;

        // Если передана строка или число - ищем по id или индексу слайда
        var slide = getSlideById(slide_as_object_or_id_or_index);

        if (slide === null)
            slide = getSlideByIndexNumber(slide_as_object_or_id_or_index);

        return (slide === null) ? false : true;
    }

    /**
     * Проверяет существование объекта слайд
     *
     * @param slide_object {jQuery}
     * @returns {boolean}
     */
    function isSlide(slide_object){
        return isBlock(slide_object, SLIDE_CLASS);
    }

    // Slide getters

    /**
     *
     * @param selector
     * @returns {*|jQuery}
     */
    function getSlideBySelector(selector){
        var slide = $(selector);
        if (!isSlide(slide)) return null;
        return slide;
    }

    /**
     *
     * @param id
     * @returns {*|jQuery|null}
     */
    function getSlideById(id, presentation){
        var slide = $('#'+id);
        if (!isSlide(slide)) return null;
        return slide;
    }

    /**
     * Находит слайд по id, порядковому номеру
     *
     * @param index {string|Number} Id | Index
     * @param presentation {jQuery} Презентация, в которой ищем слайд
     * @returns {*|jQuery|null}
     */
    function findSlideByIndexOrId(index, presentation){

        if (typeof index != "string" && typeof index != "number"){
            log('typeof index != "string" && typeof index != "number"');
            log('index = ');
            log(index);
            return null;
        }

        var slide = getSlideById(index, presentation);

        if (slide == null)
            slide = getSlideByIndexNumber(index, presentation);
        return slide;

    }

    /**
     * Возвращает слайд по порядковому номеру, начиная с 0
     *
     * @param index_number {Number|String}
     * @returns {*|jQuery}
     */
    function getSlideByIndexNumber(index_number, presentation){
        var slide = null,
            index_number = parseInt(index_number);
        if (index_number != NaN){
            slide = $(SLIDE_ON_CURRENT_PRESENTATION_SELECTOR).eq(index_number)
        }

        return isSlide(slide) ? slide : null;
    }

    // ---

    /**
     * Возвращает порядковый номер слайда, начиная с 0
     * (Нулевой/титульный слайд - без номера)
     *
     * @param slide {jQuery}
     * @returns {*|Number}
     */
    function getIndexOfSlide(slide){
        return getIndexOfBlock(slide);
    }

    // Slide Navigation

    function getNextSlide(){
        var next_slide = $(CURRENT_SLIDE_ON_CURRENT_PRESENTATION_SELECTOR).next(SLIDE_SELECTOR);
        return isSlide(next_slide) ? next_slide : null;
    }

    function getPreviousSlide(){
        var previous_slide = $(CURRENT_SLIDE_ON_CURRENT_PRESENTATION_SELECTOR).prev(SLIDE_SELECTOR);
        return isSlide(previous_slide) ? previous_slide : null;
    }

    function getCurrentSlide(){
        var current_slide = $(CURRENT_SLIDE_ON_CURRENT_PRESENTATION_SELECTOR);
        return isSlide(current_slide) ? current_slide : null;
    }

    /**
     *
     * @param slide {jQuery}
     */
    function navigateToSlide(slide){
        updateURL(slide);
        navigateToURL();
    }

    /**
     *
     * @param slide_id {String}
     * @param presentation_id {String}
     */
    function writeToURL(slide_id, presentation_id){

        var search_query = '',
            hash = '';

        // Если slide_id/presentation_id пуст, оставляем тот, что записан в URL
        if (presentation_id == null)
            presentation_id = getPresentationIdFromURL();

        if (slide_id == null)
            slide_id = getSlideIdFromURL();

        if (slide_id !== '' && slide_id !== null){
            hash = "#"+slide_id;
        }

        if (presentation_id !== '' && presentation_id !== null){
            search_query = '?presentation='+presentation_id;
        }

        history.pushState(
            {presentation: presentation_id, slide: slide_id},
            "presentation="+presentation_id + "#slide"+slide_id,
            search_query+hash
        );
    }

    /**
     * Записывает в URL (и историю) id презентации и hash слайда
     *
     * @param presentation {jQuery}
     * @param slide {jQuery}
     */
    function updateURL(slide, presentation){

        // default
        var presentation_id = 0,
            slide_id = 0,
            current_presentation = getCurrentPresentation();

        presentation_id = getPresentationId(presentation);

        slide_id = getSlideId(slide);

        writeToURL(slide_id, presentation_id);

    }

    function navigateToSlideById(id){

        var slide = findSlideByIndexOrId(id);

        updateURL(slide);

        navigateToURL();

    }

    function navigateNextSlide(){

        // Получаем следующий
        var next_slide = getNextSlide();

        updateURL(next_slide);

        navigateToURL();

    }

    function navigatePreviousSlide(){

        // Получаем предыдущий
        var previous_slide = getPreviousSlide();

        updateURL(previous_slide);

        navigateToURL();

    }

    // URL

    /**
     * Возвращает индекс или id слайда
     *
     * @param slide {jQuery}
     * @returns {*|null|number|string}
     */
    function getSlideId(slide){
        if (!slide) return null;
        var id = slide.attr('id');
        return (id != undefined) ? id : getIndexOfSlide(slide);
    }

    /**
     *
     * @param presentation {jQuery}
     * @returns {*|null|number|string}
     */
    function getPresentationId(presentation){
        if (!presentation) return null;
        var id = presentation.attr('id');
        return (id != undefined) ? id : getIndexOfPresentation(presentation);
    }

    function getIndexOfPresentation(presentation){
        return getIndexOfBlock(presentation);
    }

    function getSlideIdFromURL(){
        if (!window.location.hash) return null;

        // Получаем id из URL (удаляем #)
        var slide_id = window.location.hash.substring(1)

        return slide_id;
    }

    /**
     * Делаем слайд текущим
     *
     * @param slide {jQuery} Слайд
     */
    function makeSlideCurrent(slide){

        if (!slide) return false;

        // Снимаем модификатор "текущий" с предыдущего слайда (т.е. с текущего слайда на текущей презентации)
        var current_slide = getCurrentSlide();
        log('makeSlideCurrent start');
        log('current_slide');
        log(current_slide);
        log(typeof current_slide);

        log('slide');
        log(slide);
        log('makeSlideCurrent end');


        // презентация с новым слайдом
        var presentation = getPresentationWhichContainSlide(slide);

        // В текущей презентации есть только 1 текущий слайд
        // т.е. если есть слайд с модификатором "текущий" в данной презентации - снимаем его модификатор
        if (isPresentationCurrent(presentation)){
            current_slide.removeClass(CURRENT_SLIDE_CLASS);
        }

        // Добавляем модификатор "текущций" переданному слайду
        slide.addClass(CURRENT_SLIDE_CLASS);
    }

    function navigateToURL(){

        var default_slide = findSlideByIndexOrId(0),
            default_presentation = findPresentationByIndexOrId(0);

        // Получаем id презентации из URL
        var presentation_id = getPresentationIdFromURL();
        // Получаем презентацию
        var presentation = findPresentationByIndexOrId(presentation_id);
        makePresentationCurrent(presentation);

        // Получаем id слайда из URL
        var slide_id = getSlideIdFromURL();
        // Получаем слайд, id которого находится в URL
        var slide = findSlideByIndexOrId(slide_id, presentation);
        // Делаем слайд, id которого находится в URL, текущим
        makeSlideCurrent(slide);

        // Обновляем progress
        updateProgress();
        // Обновляем progress bar
        updateProgressBar();

    }

    //

    /**
     * Возвращает число слайдов в данной презентации
     *
     * @param presentation
     * @returns {Number}
     */
    function getSlideCountInPresentation(presentation){
        return presentation.children(SLIDE_SELECTOR).length;
    }

    function updateProgress(){
        var progress = $(PROGRESS_SELECTOR);

        if (config.progress == false){
            progress.css('display', 'none');
        }

        // динамически располагаем progress в зависимости от размеров слайдов
        var progress_x_position = config.width + config.slidePaddingLeftRight * 2;
        var progress_y_position = config.height + config.slidePaddingTopBottom * 2 - 4;// 4px высота progress bar
        progress
            // вниз презентации/слайда
            .css('top', progress_y_position)
            .css('width', progress_x_position);
    }

    /**
     * Устанавливает длину progress bar в зависимости от количества оставшихся слайдов
     */
    function updateProgressBar(){

        var progress_bar = $(PROGRESS_BAR_ON_CURRENT_PRESENTATION_SELECTOR);
        var current_slide_index = getCurrentSlide().index();
        var slide_count = getSlideCountInPresentation(getCurrentPresentation());
        var widthPercentPerSlide = 100 / (slide_count-1);

        progress_bar.
            css('width', widthPercentPerSlide * current_slide_index + '%')

    }

    // Presentations

    /**
     *
     * @param index {string|number}
     * @returns {*|null|jQuery}
     */
    function findPresentationByIndexOrId(index){
        if (typeof index != "string" && typeof  index != "number"){
            return false;
        }

        var presentation = getPresentationById(index);

        if (presentation == null)
            presentation = getPresentationByIndex(index)

        // в presentation получаем либо презентацию, либо null, если презентация не найдена/не существует
        return presentation;

    }

    function getPresentationIdFromURL(){
        if (!window.location.hash) return null;

        // Получаем id из URL
        var id_start_position = window.location.search.indexOf('=') + 1;
        var presentation_id = window.location.search.substring(id_start_position);

        if (presentation_id == -1) stackTrace();
        return presentation_id;
    }

    function howManyPresentationsOnPage(){
        var amount = $(PRESENTATION_SELECTOR).length;
        return amount;
    }

    function isPresentation(presentation_object){
        if (!presentation_object) return false;
        return isBlock(presentation_object, PRESENTATION_CLASS);
    }

    function isPresentationCurrent(presentation){
        if (!presentation) return false;
        return isBlock(presentation, CURRENT_PRESENTATION_CLASS);
    }

    /**
     *
     * @param presentation_as_obj_or_id
     * @returns {boolean}
     */
    function isPresentationExists(presentation_as_obj_or_id){

        // Если передан презентация - проверяем ее как объект
        if (isPresentation(presentation_as_obj_or_id)) return true;

        // Если передана строка или число - ищем по id или индексу
        var presentation = findPresentationByIndexOrId(presentation_as_obj_or_id);
        if (presentation) return true;

        return false;
    }

    // Block

    /**
     *
     * @param block {jQuery}
     * @returns {null}
     */
    function getIndexOfBlock(block){
        var block_index = block.index();
        return (block_index == -1) ? null : block_index;
    }

    /**
     * Является ли block блоком класса block_class
     *
     * @param block {string}
     * @param block_class {string}
     * @returns {*}
     */
    function isBlock(block, block_class){

        if (!block || !block_class) return false;

        // Если передан объект (блок)
        if (block instanceof Object){
            // Объект (блок) не пуст и имеет класс block_class
            return (block.length != 0) && (block.hasClass(block_class));
        }

        return false;

    }

    // Presentation getters


    function getPresentationWhichContainSlide(slide){

        var presentation = null;

        presentation = slide.parent(PRESENTATION_SELECTOR);

        return isPresentation(presentation) ? presentation : null;
    }

    /**
     * Возвращает элемент Презентация по его индексу
     *
     * @param index {number} Индекс(порядковый номер) презентации, начиная с 0
     * @returns {*}
     */
    function getPresentationByIndex(index){
        var p = null,
            index = parseInt(index);
        if (index != NaN){
            p = $(PRESENTATION_SELECTOR).eq(index)
        }

        return isPresentationExists(p) ? p : null;
    }

    function getPresentationById(id){
        var presentation = $('#'+id);
        if (!isPresentationExists(presentation)) return null;
        return presentation;
    }


    function getCurrentPresentation(){
        var current_presentation = $(CURRENT_PRESENTATION_SELECTOR);
        return current_presentation;
    }

    function makePresentationCurrent(presentation){

        if (!presentation) return false;

        // Снимаем модификатор "текущий" с предыдущей презентации
        var current_presentation = getCurrentPresentation();

        current_presentation.removeClass(CURRENT_PRESENTATION_CLASS);

        // Добавляем модификатор "текущций" переданной презентации
        presentation.addClass(CURRENT_PRESENTATION_CLASS);

    }

    function getNextPresentation(){
        var next_presentation = $(CURRENT_PRESENTATION_SELECTOR).next(PRESENTATION_SELECTOR);
        return isPresentationExists(next_presentation) ? next_presentation : null;
    }

    function getPreviousPresentation(){
        var previous_presentation = $(CURRENT_PRESENTATION_SELECTOR).prev(PRESENTATION_SELECTOR);
        return isPresentationExists(previous_presentation) ? previous_presentation : null;
    }

    // Presentation Navigation

    /**
     * Возвращает текущий слайд в данной презентации
     *
     * @param presentation {jQuery} Принимает блок Презентация
     */
    function getCurrentSlideOfPresentation(presentation){
        return presentation.children(CURRENT_SLIDE_SELECTOR);
    }


    /**
     *
     * @param round {boolean} Чередовать ли выбор по кругу
     */
    function navigateNextPresentation(round){

        var next_presentation = getNextPresentation();

        // Если "по кругу" и текущая презентация является последней - начинаем заново
        if (round && next_presentation == null){
            next_presentation = getPresentationByIndex(0);
        }

        var slide = getCurrentSlideOfPresentation(next_presentation);

        updateURL(slide, next_presentation);

        navigateToURL();

    }

    /**
     * Applies JavaScript-controlled layout rules to the
     * presentation.
     */
    function layout(){
        var slideWidth = config.width,
            slideHeight = config.height;

        // Задаем размеры слайдов
        var slides_list = getSlideBySelector(SLIDE_SELECTOR);

        log('slideWidth');
        log(slideWidth);

        log('slideHeight');
        log(slideHeight);


        slides_list
            .css('width', slideWidth+'px')
            .css('height', slideHeight+'px');

    }

    // --------------------------------------------------------------------//
    // ----------------------------- EVENTS -------------------------------//
    // --------------------------------------------------------------------//

    function onSlideClick(event){

        // 1) Получаем выбранные слайд и презентацию
        var selected_slide = $(this);

        log('selected_slide');
        log(selected_slide);
//        var selected_presentation = $(this).parent(PRESENTATION_SELECTOR);
        var selected_presentation = getPresentationWhichContainSlide(selected_slide);


        // 2) Обновляем их id в URL
        updateURL(selected_slide, selected_presentation);

        // 3) Переходим по новому URL
        navigateToURL();

    }

    function onPresentationClick(event){

        // Выбрана презентация, по которой кликнули
        var selected_presentation = $(this);

        updateURL(null, selected_presentation);

        navigateToURL();

    }

    /**
     * Изменен hash в URL (/index.html#slide1 -> #slide2)
     *
     * @param event
     */
    function onHashChanged(event){

        navigateToURL();

    }

    function onDocumentKeyDown(event){

        switch( event.keyCode ) {
            // TAB
            case 9:
                event.preventDefault();
                // Выбор презентаций по кругу
                navigateNextPresentation(true);
                break;

            // left
            case 37:
                event.preventDefault();
                navigatePreviousSlide();
                break;
            // right
            case 39:
                event.preventDefault();
                navigateNextSlide();
                break;
            // home
            case 36:
                event.preventDefault();
                navigateToSlide( getSlideBySelector(HOME_SLIDE_ON_CURRENT_PRESENTATION_SELECTOR) );
                break;
            // end
            case 35:
                event.preventDefault();
                navigateToSlide( getSlideBySelector(END_SLIDE_SELECTOR) );
                break;
            default:
                triggered = false;
        }

    }

    /**
     * Привязываем события
     */
    function addEventListeners(){

        // Клик по слайду
        $(SLIDE_SELECTOR).on('click', onSlideClick);

        // Изменение хэша
        $(window).on('hashchange', onHashChanged);

        document.addEventListener( 'keydown', onDocumentKeyDown, false );

        // Force a layout when the whole page, incl fonts, has loaded
        window.addEventListener( 'load', layout, false );

    }

    function configure(){

        addEventListeners();

    }

    function start(){

        // Начинает показ с указанного в URL слайда
        var slide_id = getSlideIdFromURL();
        navigateToSlideById(slide_id);

        configure();

    }

	// --------------------------------------------------------------------//
	// ------------------------------- API --------------------------------//
	// --------------------------------------------------------------------//

	return {
		initialize: initialize,
        start: start,
        getNextSlide: getNextSlide,
        getPreviousSlide: getPreviousSlide,
        getCurrentSlide: getCurrentSlide,
        getCurrentPresentation: getCurrentPresentation,
        getNextPresentation: getNextPresentation,
        updateProgressBar: updateProgressBar,
	}
})();